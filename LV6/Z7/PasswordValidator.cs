﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Z7
{
    class PasswordValidator
    {
        private StringChecker firstChecker;
        private StringChecker lastChecker;

        public PasswordValidator(StringChecker checker)
        {
            this.firstChecker = checker;
            this.lastChecker = checker;
        }

        public void addChecker(StringChecker newChecker)
        {
            this.lastChecker.SetNext(newChecker);
            this.lastChecker = newChecker;
        }

        public bool checkPassword(string password)
        {
            return this.firstChecker.Check(password);
        }
    }
}
