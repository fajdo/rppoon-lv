﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Z7
{
    class Program
    {
        static void Main(string[] args)
        {
            StringDigitChecker digitChecker = new StringDigitChecker();
            StringLengthChecker lengthChecker = new StringLengthChecker(6);
            StringUpperCaseChecker upperCaseChecker = new StringUpperCaseChecker();
            StringLowerCaseChecker lowerCaseChecker = new StringLowerCaseChecker();

            PasswordValidator validator = new PasswordValidator(digitChecker);
            validator.addChecker(lengthChecker);
            validator.addChecker(upperCaseChecker);
            validator.addChecker(lowerCaseChecker);

            Console.WriteLine(validator.checkPassword("abcdABC1"));
            Console.WriteLine(validator.checkPassword("aB1sc"));
        }
    }
}
