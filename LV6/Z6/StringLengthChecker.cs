﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Z6
{
    class StringLengthChecker : StringChecker
    {
        private int minLength;

        public StringLengthChecker(int minLength) : base()
        {
            this.minLength = minLength;
        }

        protected override bool PerformCheck(string stringToCheck)
        {
            return stringToCheck.Length >= this.minLength;
        }
    }
}
