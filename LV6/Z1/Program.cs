﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Z1
{
    class Program
    {
        static void Main(string[] args)
        {
            List<Note> notes = new List<Note>();
            notes.Add(new Note("Note 1", "Text 1"));
            notes.Add(new Note("Note 2", "Text 2"));
            notes.Add(new Note("Note 3", "Text 3"));
            Notebook notebook = new Notebook(notes);
            IAbstractIterator iterator = notebook.GetIterator();
            for(Note note = iterator.First(); !iterator.IsDone; note = iterator.Next())
            {
                note.Show();
            }
        }
    }
}
