﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Z2
{
    class Program
    {
        static void Main(string[] args)
        {
            List<Product> products = new List<Product>();
            products.Add(new Product("Product 1", 2));
            products.Add(new Product("Product 2", 5.5));
            products.Add(new Product("Product 3", 10));
            Box box = new Box(products);
            IAbstractIterator iterator = box.GetIterator();
            for (Product product = iterator.First(); !iterator.IsDone; product = iterator.Next())
            {
                Console.WriteLine(product.ToString());
            }
        }
    }
}
