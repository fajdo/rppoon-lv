﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Z5
{
    class Program
    {
        static void Main(string[] args)
        {
            AbstractLogger logger = new ConsoleLogger(MessageType.ALL);
            FileLogger fileLogger = new FileLogger(MessageType.ERROR | MessageType.WARNING, "logFile.txt");

            logger.SetNextLogger(fileLogger);

            logger.Log("Test message 1",MessageType.INFO);
            logger.Log("Test message 2", MessageType.ERROR);
            logger.Log("Test message 3", MessageType.WARNING);
        }
    }
}
