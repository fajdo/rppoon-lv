﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Z1
{
    class Program
    {
        static void Main(string[] args)
        {
            double[] numbers = new double[] { 3, 6, 4, 2, 10, 1, 5, 7, 8, 0, 9 };
            NumberSequence sequentialSequence = new NumberSequence(numbers);
            sequentialSequence.SetSortStrategy(new SequentialSort());
            NumberSequence bubbleSequence = new NumberSequence(numbers);
            bubbleSequence.SetSortStrategy(new BubbleSort());
            NumberSequence combSequence = new NumberSequence(numbers);
            combSequence.SetSortStrategy(new CombSort());

            sequentialSequence.Sort();
            bubbleSequence.Sort();
            combSequence.Sort();

            Console.WriteLine(sequentialSequence.ToString());
            Console.WriteLine(bubbleSequence.ToString());
            Console.WriteLine(combSequence.ToString());
        }
    }
}
