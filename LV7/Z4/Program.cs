﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace Z4
{
    class Program
    {
        static void Main(string[] args)
        {
            SystemDataProvider dataProvider = new SystemDataProvider();
            dataProvider.Attach(new FileLogger("test.txt"));
            dataProvider.Attach(new ConsoleLogger());
            while (true)
            {
                dataProvider.GetCPULoad();
                dataProvider.GetAvailableRAM();
                Thread.Sleep(1000);
            }
        }
    }
}
