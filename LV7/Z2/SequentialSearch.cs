﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Z2
{
    class SequentialSearch : ISearchStrategy
    {
        public bool Search(double[] array, double element)
        {
            for (int i = 0; i < array.Length; i++)
                if (array[i] == element)
                    return true;
            return false;
        }
    }
}
