﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Z2
{
    class Program
    {
        static void Main(string[] args)
        {
            double[] numbers = new double[] { 3, 6, 4, 2, 10, 1, 5, 7, 8, 0, 9 };
            NumberSequence numberSequence = new NumberSequence(numbers);
            numberSequence.SetSearchStrategy(new SequentialSearch());

            Console.WriteLine(numberSequence.Search(5));
            Console.WriteLine(numberSequence.Search(15));
        }
    }
}
