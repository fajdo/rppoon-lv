﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Z2
{
    class ShippingService
    {
        private double pricePerKg { get; set; }

        public ShippingService(double price)
        {
            pricePerKg = price;
        }

        public double calculateDeliveryPrice(IShippable package)
        {
            return package.Weight * pricePerKg;
        }
    }
}
