﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Z1
{
    interface IShippable
    {
        double Price { get; }
        double Weight { get; }
        string Description(int depth = 0);
    }
}
