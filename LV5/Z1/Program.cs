﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Z1
{
    class Program
    {
        static void Main(string[] args)
        {
            Product ball = new Product("ball", 10, 0.5);
            Product toy = new Product("Toy", 30, 0.2);
            Product tv = new Product("Tv", 3000, 7);
            Box box = new Box("Box 1");
            box.Add(ball);
            box.Add(toy);
            box.Add(tv);
            Console.WriteLine(box.Description());
        }
    }
}
