﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Z3
{
    class Program
    {
        static void Main(string[] args)
        {
            DataConsolePrinter printer = new DataConsolePrinter();

            VirtualProxyDataset virtualProxy = new VirtualProxyDataset("test.csv");
            printer.printData(virtualProxy);

            User luka = User.GenerateUser("Luka");
            User matej = User.GenerateUser("Matej");
            ProtectionProxyDataset lukaProxy = new ProtectionProxyDataset(luka);
            ProtectionProxyDataset matejProxy = new ProtectionProxyDataset(matej);
            printer.printData(lukaProxy);
            printer.printData(matejProxy);
        }
    }
}
