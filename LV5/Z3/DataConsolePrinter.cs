﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Z3
{
    class DataConsolePrinter
    {
        public void printData(IDataset dataset)
        {
            if (dataset.GetData() == null)
            {
                Console.WriteLine("Data not accessible");
                return;
            }
            StringBuilder stringBuilder = new StringBuilder();
            foreach(List<string> row in dataset.GetData())
            {
                foreach(string word in row)
                {
                    stringBuilder.Append(word).Append(' ');
                }
                stringBuilder.Append('\n');
            }
            Console.WriteLine(stringBuilder.ToString());
        }
    }
}
