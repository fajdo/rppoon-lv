﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Z5
{
    class Program
    {
        static void Main(string[] args)
        {
            Note firstNote = new ReminderNote("Do something", new LightTheme());
            firstNote.Show();

            Note secondNote = new ReminderNote("Do something again", new DarkTheme());
            secondNote.Show();
        }
    }
}
