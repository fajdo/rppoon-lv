﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Z4
{
    class DataConsolePrinter
    {
        public void printData(IDataset dataset)
        {
            ReadOnlyCollection<List<string>> data = dataset.GetData();
            if (data == null)
            {
                Console.WriteLine("Data not accessible");
                return;
            }
            StringBuilder stringBuilder = new StringBuilder();
            foreach(List<string> row in data)
            {
                foreach(string word in row)
                {
                    stringBuilder.Append(word).Append(' ');
                }
                stringBuilder.Append('\n');
            }
            Console.WriteLine(stringBuilder.ToString());
        }
    }
}
