﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Z4
{
    class LoggingProxyDataset : IDataset
    {
        private string filePath;
        private Dataset dataset;
        public LoggingProxyDataset(string filePath)
        {
            this.filePath = filePath;
            this.dataset = new Dataset(filePath);
        }
        public ReadOnlyCollection<List<string>> GetData()
        {
            ConsoleLogger.getInstance().log("Data accessed at " + DateTime.Now);
            return dataset.GetData();
        }
    }
}
