﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Z4
{
    class ConsoleLogger
    {
        private static ConsoleLogger instance;

        private ConsoleLogger() { }

        public static ConsoleLogger getInstance()
        {
            if (instance == null)
                instance = new ConsoleLogger();
            return instance;
        }

        public void log(string data)
        {
            Console.WriteLine(data);
        }
    }
}
