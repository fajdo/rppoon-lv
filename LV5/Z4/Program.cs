﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Z4
{
    class Program
    {
        static void Main(string[] args)
        {
            LoggingProxyDataset proxy = new LoggingProxyDataset("test.csv");
            DataConsolePrinter printer = new DataConsolePrinter();
            printer.printData(proxy);
        }
    }
}
