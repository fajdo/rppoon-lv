﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Z2
{
    class Program
    {
        static void Main(string[] args)
        {
            Dataset dataset = new Dataset("test.txt");
            Analyzer3rdParty analyzer = new Analyzer3rdParty();
            Adapter adapter = new Adapter(analyzer);
            double[] rowAverages = adapter.CalculateAveragePerRow(dataset);
            double[] columnAverages = adapter.CalculateAveragePerColumn(dataset);
            Console.WriteLine("Row averages:");
            foreach (double number in rowAverages)
                Console.WriteLine(number);
            Console.WriteLine("Column averages:");
            foreach (double number in columnAverages)
                Console.WriteLine(number);
        }
    }
}
