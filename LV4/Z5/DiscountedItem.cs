﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Z5
{
    class DiscountedItem : RentableDecorator
    {
        private readonly double DiscountPercentage;
        public DiscountedItem(IRentable rentable, double discount) : base(rentable) {
            this.DiscountPercentage = discount;
        }
        public override double CalculatePrice()
        {
            return base.CalculatePrice() * (100 - DiscountPercentage) / 100 ;
        }
        public override String Description
        {
            get
            {
                return base.Description + " now at " + DiscountPercentage + "% off!";
            }
        }
    }
}
