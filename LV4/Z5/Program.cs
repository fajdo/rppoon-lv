﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Z5
{
    class Program
    {
        static void Main(string[] args)
        {
            List<IRentable> rentables = new List<IRentable>();
            rentables.Add(new Video("Video 1"));
            rentables.Add(new Book("Book 1"));
            rentables.Add(new HotItem(new Video("Pop video")));
            rentables.Add(new HotItem(new Book("Pop book")));

            List<IRentable> flashSale = new List<IRentable>();
            foreach(IRentable rentable in rentables)
            {
                flashSale.Add(new DiscountedItem(rentable, 10));
            }

            RentingConsolePrinter printer = new RentingConsolePrinter();
            printer.DisplayItems(flashSale);
            printer.PrintTotalPrice(flashSale);
        }
    }
}
