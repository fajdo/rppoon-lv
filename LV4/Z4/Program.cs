﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Z4
{
    class Program
    {
        static void Main(string[] args)
        {
            List<IRentable> rentables = new List<IRentable>();
            rentables.Add(new Video("Video 1"));
            rentables.Add(new Book("Book 1"));
            rentables.Add(new HotItem(new Video("Pop video")));
            rentables.Add(new HotItem(new Book("Pop book")));
            RentingConsolePrinter printer = new RentingConsolePrinter();
            printer.DisplayItems(rentables);
            printer.PrintTotalPrice(rentables); 
            //razlika je to što kod hotItema piše trending: ispred naziva knjige/filma i što se uračunava bonus u cijenu
        }
    }
}
