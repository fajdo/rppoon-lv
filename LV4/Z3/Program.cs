﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Z3
{
    class Program
    {
        static void Main(string[] args)
        {
            List<IRentable> rentables = new List<IRentable>();
            rentables.Add(new Video("Video 1"));
            rentables.Add(new Book("Book 1"));
            RentingConsolePrinter printer = new RentingConsolePrinter();
            printer.DisplayItems(rentables);
            printer.PrintTotalPrice(rentables);
        }
    }
}
