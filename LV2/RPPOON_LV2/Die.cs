﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RPPOON_LV2
{
    class Die
    {
        private int numberOfSides;
        //Z1,Z2
        //private Random randomGenerator;
        //Z1
        /*public Die(int numberOfSides)
        {
            this.numberOfSides = numberOfSides;
            this.randomGenerator = new Random();
        }*/
        //Z2
        /*public Die(int numberOfSides, Random generator)
        {
            this.numberOfSides = numberOfSides;
            this.randomGenerator = generator;
        }*/
        public Die(int numberOfSides)
        {
            this.numberOfSides = numberOfSides;
        }
        public int Roll()
        {
            //Z1,Z2
            //int rolledNumber = randomGenerator.Next(1, numberOfSides + 1);
            int rolledNumber = RandomGenerator.GetInstance().NextInt(1, numberOfSides + 1);
            return rolledNumber;
        }
    }
}
