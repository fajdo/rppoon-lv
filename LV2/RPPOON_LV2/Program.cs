﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RPPOON_LV2
{
    class Program
    {
        static void Main(string[] args)
        {
            ILogger logger = new ConsoleLogger();
            //Z1,Z2,Z3
            //DiceRoller diceRoller = new DiceRoller();
            DiceRoller diceRoller = new DiceRoller(logger);
            //Z2
            //Random random = new Random();
            for (int i = 0; i < 20; i++)
            {
                diceRoller.InsertDie(new Die(6));
                //Z2
                //diceRoller.InsertDie(new Die(6, random));
            }
            diceRoller.RollAllDice();
            //Z1,Z2,Z3
            //diceRoller.printRollingResults();
            diceRoller.logRollingResults();
        }
    }
}
