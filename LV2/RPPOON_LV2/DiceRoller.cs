﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RPPOON_LV2
{
    class DiceRoller : ILoggable
    {
        private List<Die> dice;
        private List<int> resultForEachRoll;
        private ILogger logger;
        //Z1,Z2,Z3
        /*public DiceRoller()
        {
            this.dice = new List<Die>();
            this.resultForEachRoll = new List<int>();
        }*/
        public DiceRoller(ILogger logger)
        {
            this.dice = new List<Die>();
            this.resultForEachRoll = new List<int>();
            this.logger = logger;
        }
        public void InsertDie(Die die)
        {
            dice.Add(die);
        }
        public void RollAllDice()
        {
            //clear results of previous rolling
            this.resultForEachRoll.Clear();
            foreach (Die die in dice)
            {
                this.resultForEachRoll.Add(die.Roll());
            }
        }
        //View of the results
        public IList<int> GetRollingResults()
        {
            return new System.Collections.ObjectModel.ReadOnlyCollection<int>(
           this.resultForEachRoll
           );
        }
        //Z1,Z2
        /*public void printRollingResults()
        {
            foreach (int result in resultForEachRoll)
                Console.WriteLine(result);
        }*/
        public string GetStringRepresentation()
        {
            StringBuilder builder = new StringBuilder();
            foreach (int result in resultForEachRoll)
                builder.Append(result).Append(' ');
            return builder.ToString();
        }
        public void logRollingResults()
        {
            //Z4
            /*
            StringBuilder builder = new StringBuilder();
            foreach (int result in resultForEachRoll)
                builder.Append(result).Append(' ');
            logger.Log(builder.ToString());
            */
            logger.Log(this);
        }
        public int DiceCount
        {
            get { return dice.Count; }
        }
    }
}
