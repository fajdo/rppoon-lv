﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RPPOON_LV1
{
    class Note
    {
        private String text;
        private String author;
        private int importanceLevel;

        public Note(String text, String author, int importance)
        {
            this.text = text;
            this.author = author;
            this.importanceLevel = importance;
        }
        public Note(String author, int importance)
        {
            this.text = String.Empty;
            this.author = author;
            this.importanceLevel = importance;
        }
        public Note(String author)
        {
            this.text = String.Empty;
            this.author = author;
            this.importanceLevel = 1;
        }

        public String getText()
        {
            return text;
        }
        public String getAuthor()
        {
            return author;
        }
        public int getImportanceLevel()
        {
            return importanceLevel;
        }
        public void setText(String text)
        {
            this.text = text;
        }
        public void setImportanceLevel(int importance)
        {
            this.importanceLevel = importance;
        }

        public String Text
        {
            get { return this.text; }
            set { this.text = value; }
        }
        public String Author
        {
            get { return this.author; }
            private set { this.author = value; }
        }
        public int ImportanceLevel
        {
            get { return this.importanceLevel; }
            set { this.importanceLevel = value; }
        }

        public override string ToString()
        {
            return this.author + ": " + this.text;
        }
    }
}
