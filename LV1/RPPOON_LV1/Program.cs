﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RPPOON_LV1
{
    class Program
    {
        static void Main(string[] args)
        {
            Note firstNote = new Note("Asdf", "Luka", 5);
            Note secondNote = new Note("Luka", 3);
            Note thirdNote = new Note("L.Fajdetic");

            //Console.WriteLine(firstNote.getAuthor() + ": " + firstNote.getText());
            //Console.WriteLine(secondNote.getAuthor() + ": " + secondNote.getText());
            //Console.WriteLine(thirdNote.getAuthor() + ": " + thirdNote.getText());

            Console.WriteLine(firstNote.ToString());
            Console.WriteLine(secondNote.ToString());
            Console.WriteLine(thirdNote.ToString());
        }
    }
}
