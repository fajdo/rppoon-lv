﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Z4
{
    class Program
    {
        static void Main(string[] args)
        {
            ConsoleNotification firstNotification =
                new ConsoleNotification("Luka", "First", "asdfasdf", DateTime.Now, Category.INFO, ConsoleColor.Blue);
            ConsoleNotification secondNotification =
                new ConsoleNotification("Luka", "Second", "qwertz", DateTime.Now, Category.ERROR, ConsoleColor.Green);
            NotificationManager manager = new NotificationManager();
            manager.Display(firstNotification);
            manager.Display(secondNotification);
        }
    }
}
