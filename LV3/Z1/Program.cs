﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Z1
{
    class Program
    {
        static void printData(IList<List<string>> data)
        {
            StringBuilder builder = new StringBuilder();
            foreach (List<string> row in data)
            {

                foreach (string word in row)
                {
                    builder.Append(word).Append(" ");
                }
                builder.Append("\n");
            }
            Console.WriteLine(builder.ToString());
        }

        static void Main(string[] args)
        {
            Dataset dataset = new Dataset("Test.txt");
            IList<List<string>> data = dataset.GetData();
            Dataset clone = (Dataset)dataset.Clone();
            printData(data);
            dataset.ClearData();
            IList<List<string>> cloneData = clone.GetData();
            printData(cloneData);

            //potrebno je duboko kopiranje jer bi inace i originalni objekt i njegova kopija imali reference na iste stringove
            //i liste stringova
        }
    }
}
