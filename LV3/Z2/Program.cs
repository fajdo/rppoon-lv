﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Z2
{
    class Program
    {
        static void Main(string[] args)
        {
            double[][] matrix = MatrixGenerator.getInstance().createMatrix(5, 8);
            StringBuilder builder = new StringBuilder();
            for(int i = 0; i < 5; i++)
            {
                for(int j = 0; j < 8; j++)
                {
                    builder.Append(matrix[i][j]).Append(' ');
                }
                builder.Append('\n');
            }
            Console.WriteLine(builder.ToString());
        }
    }
}
