﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Z5
{
    class Program
    {
        static void Main(string[] args)
        {
            NotificationBuilder builder = new NotificationBuilder();
            builder.SetAuthor("Luka").SetLevel(Category.ALERT).SetText("Random text").SetTitle("Z5").SetColor(ConsoleColor.Magenta);
            ConsoleNotification notification = builder.Build();
            NotificationManager manager = new NotificationManager();
            manager.Display(notification);
        }
    }
}
