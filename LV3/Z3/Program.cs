﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Z3
{
    class Program
    {
        static void Main(string[] args)
        {
            Logger logger = Logger.getInstance();
            logger.filePath = "z3.txt";
            logger.Log("Test test test");
            logger.Log("Test 2");

            //Ako je datoteka postavljena na jednom mjestu u tekstu programa, hoće li uporaba loggera na
            //drugim mjesta u testu programa pisati u istu datoteku(pretpostavka je kako nisu ponovo postavljene) ?

            //Hoće jer postoji samo jedna instanca loggera
        }
    }
}
